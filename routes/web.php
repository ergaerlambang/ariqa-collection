<?php
// **************************************************
// **************** Route Login *********************
// **************************************************
Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function() {
    // Login
    Route::get('/signin','LoginController@index')->name('auth.login');
    Route::post('/signin/post','LoginController@post')->name('auth.login.post');
    Route::get('/logout','LoginController@logout')->name('auth.logout');
});

// **************************************************
// **************** Route Admin *********************
// **************************************************
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function() {
    // Dashboard
    Route::get('/','IndexController@dashboard')->name('dashboard');

    // Banner Management
    Route::group(['prefix' => 'banner'],function() {
        Route::get('/','BannerController@index')->name('banner');
        Route::get('/create','BannerController@create')->name('banner.create');
        Route::post('/store','BannerController@store')->name('banner.store');
        Route::get('/edit/{id}','BannerController@edit')->name('banner.edit');
        Route::post('/update/{id}','BannerController@update')->name('banner.update');
        Route::post('/destroy/{id}','BannerController@destroy')->name('banner.destroy');
        Route::get('/thumbnail','BannerController@thumbnail')->name('banner.thumbnail');
    });

    // About Management
    Route::group(['prefix' => 'about'],function() {
        Route::get('/','AboutController@index')->name('about');
        Route::get('/create','AboutController@create')->name('about.create');
        Route::post('/store','AboutController@store')->name('about.store');
        Route::get('/edit/{id}','AboutController@edit')->name('about.edit');
        Route::post('/update/{id}','AboutController@update')->name('about.update');
        Route::post('/destroy/{id}','AboutController@destroy')->name('about.destroy');
        Route::get('/thumbnail','AboutController@thumbnail')->name('about.thumbnail');
    });

    // Product Management
    Route::group(['prefix' => 'product'],function() {
        Route::get('/','ProductController@index')->name('product');
        Route::get('/create','ProductController@create')->name('product.create');
        Route::post('/store','ProductController@store')->name('product.store');
        Route::get('/edit/{id}','ProductController@edit')->name('product.edit');
        Route::post('/update/{id}','ProductController@update')->name('product.update');
        Route::post('/destroy/{id}','ProductController@destroy')->name('product.destroy');
        Route::get('/thumbnail','ProductController@thumbnail')->name('product.thumbnail');
    });

    // Testimonial Management
    Route::group(['prefix' => 'testimonial'],function() {
        Route::get('/','TestimoniController@index')->name('testimoni');
        Route::get('/create','TestimoniController@create')->name('testimoni.create');
        Route::post('/store','TestimoniController@store')->name('testimoni.store');
        Route::get('/edit/{id}','TestimoniController@edit')->name('testimoni.edit');
        Route::post('/update/{id}','TestimoniController@update')->name('testimoni.update');
        Route::post('/destroy/{id}','TestimoniController@destroy')->name('testimoni.destroy');
        Route::get('/thumbnail','TestimoniController@thumbnail')->name('testimoni.thumbnail');
    });

    // Contact Management
    Route::group(['prefix' => 'contact'],function() {
        Route::get('/','ContactController@index')->name('contact');
        Route::get('/create','ContactController@create')->name('contact.create');
        Route::post('/store','ContactController@store')->name('contact.store');
        Route::get('/edit/{id}','ContactController@edit')->name('contact.edit');
        Route::post('/update/{id}','ContactController@update')->name('contact.update');
        Route::post('/destroy/{id}','ContactController@destroy')->name('contact.destroy');
        Route::get('/thumbnail','ContactController@thumbnail')->name('contact.thumbnail');
    });
});

// **************************************************
// ************** Route Frontend ********************
// **************************************************
Route::group(['namespace' => 'Frontend'], function() {
    // Home / Beranda
    Route::get('/','IndexController@home')->name('home');
});

