<?php

use Illuminate\Database\Seeder;
use App\Contact;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('contact')->truncate();
        Schema::enableForeignKeyConstraints();

        $data = [
            [
                "name" => "Instagram",
                "link" => "https://instagram.com/ariqa.collection?utm_medium=copy_link",
                "file" => "ig.jpg"
            ],
            [
                "name" => "TikTok",
                "link" => "https://vt.tiktok.com/ZSeh3HWho/",
                "file" => "tt.jpg"
            ],
            [
                "name" => "WhatsApp",
                "link" => "https://wa.me/6285821141725",
                "file" => "wa.jpg"
            ],
            [
                "name" => "Shopee",
                "link" => "https://shopee.co.id/ariqacollection?smtt=0.0.9",
                "file" => "sp.jpg"
            ],
        ];

        foreach($data as $contact) {
            $app = new Contact;
            $app->name = $contact['name'];
            $app->link = $contact['link'];
            $app->file = $contact['file'];
            $app->save();
        }
    }
}
