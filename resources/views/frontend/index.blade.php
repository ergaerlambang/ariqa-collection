<!DOCTYPE html>
<!--
	xBe by TEMPLATE STOCK
	templatestock.co @templatestock
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->


<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    

    <title> AriqaCollection</title>

    <!-- Bootstrap Core CSS -->

    <link href="{{asset('assets/frontend/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Custom CSS -->

    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.css')}}">

    <!-- Custom Fonts -->

   <link href="{{asset('assets/frontend/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
   <link href='https://fonts.googleapis.com/css?family=Lato:400,300,100,700,900' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>


    <!-- Custom Animations -->

    <link rel="stylesheet" href="{{asset('assets/frontend/css/animate.css')}}">

</head>

<body>
    <!-- HEADER -->
    <header id="header">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top navbar-shrink">
            <div class="container-fluid top-nav">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden nav-buttons">
                            <a href="#page-top"></a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#team">Tentang Kami</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#portfolio">Produk Best Seller</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#testimonials">Testimonials</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Kontak</a>
                        </li>
                        {{-- <li>
                            <a href="{{ route('auth.login') }}">Login</a>
                        </li> --}}
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>
    <!-- Slider -->
    <section id="slider" style="margin-top:3.6em !important">
      <div id="myCarousel-one" class="carousel slide">

            <ol class="carousel-indicators">
                @foreach ($data['banner'] as $key => $banner)
                    <li data-target="#myCarousel-one" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
                @endforeach
            </ol>

            <div class="carousel-inner">
                @foreach ($data['banner'] as $key => $banner)
                    <div class="item {{ $key == 0 ? "active" : "" }}"> 
                        <div class="carousel-caption wrapper">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="intro-text">
                                        <h1 class="intro-lead-in animated bounceInLeft">{{ ucwords($banner->title) }}</h1>
                                        <h2 class="intro-heading animated bounceInRight">{{ ucwords($banner->description) }}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img src="{{asset('uploads/banner/'.$banner->file)}}" alt="slider-image" class="img-fluid" style="width: 1500px; height:1000px"/>
                    </div>
                @endforeach
                <!-- Controls -->
                <a class="myCarousel-one-left" href="#myCarousel-one" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="myCarousel-one-right" href="#myCarousel-one" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </section>

    <!-- Services Section -->

    <!-- Team Section -->
    <section id="team">
        <div class="container-fluid wrapper">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Tentang Kami</h2>
                    <h3 class="section-subheading text-muted"> 
                         
                    </div>
            </div>
            <div id="myCarousel-two" class="carousel slide">
                <!-- Wrapper for Slides -->
                <div class="carousel-inner team-wrapper">
                    @foreach($data['about'] as $key => $about)
                        <div class="item {{ $key == 0 ? "active" : "" }}">
                            <!-- Team Member One -->
                            <div class="col-xs-12 col-sm-4 col-md-4 team-member">
                                <img src="{{ asset('uploads/about/'.$about->file) }}" alt="team-member-img1" width="400" height="600">
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-8 team-member-bio">
                                <h3 class="team-member-name"> </h3>
                                <p class="text-muted-role"> </p>
                                <p class="team-text-short">{{ $about->description }}</p>
                                
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#myCarousel-two" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#myCarousel-two" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    @foreach($data['about'] as $key => $about)
                        <li data-target="#myCarousel-two" data-slide-to="{{ $key }}" class="{{ $key == 0 ? "active" : "" }}"></li>
                    @endforeach
                </ol>
            </div>
        </div>
    </section>
    <!-- Portfolio Section -->
    <section id="portfolio">
        <div class="container-fluid wrapper">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-heading">Produk Best Seller</h2>
                    <h3 class="section-subheading text-muted"> </h3>
                </div>
                    <!--/#portfolio-filter-->
                </div>
            </div>
        </div>
        <div class="portfolio-wrapper portfolio-container-fluid">
            <div class="portfolio-items">
                @foreach ($data['product'] as $key => $product)
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 work-grid php graphic">
                        <div class="portfolio-item">
                            <div class="hover-bg">
                                <a href="{{ $product->link }}" target="_blank">
                                    <div class="hover-text">
                                        <h4>{{ $product->name }}</h4>
                                        <h5>#Best Seller {{ $key+1 }}</h5>
                                        <div class="clearfix"></div>
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <img src="{{asset('uploads/product/'.$product->file)}}" class="img-responsive" alt="portfolio-image">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach 
            </div>
        </div>
    </section>

    <!-- Testimonials Section-->
    <section id="testimonials">
        <div class="container-fluid wrapper">
            <div class="row">
                <div class="col-sm-8 col-lg-12 text-left">
                    <h2 class="section-heading">Testimonials</h2>
                    <h3 class="section-subheading">Berikut merupakan beberapa testimonial terbaik berdasarkan penjualan melalui Shopee.</h3>
                </div>
            </div>
            <div id="myCarousel-four" class="carousel slide">
                <br>
                <ol class="carousel-indicators" style="bottom: 0;">
                    @foreach ($data['testimoni']->chunk(2) as $key => $testimoni)
                         <li data-target="#myCarousel-four" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
                    @endforeach
                 </ol>
         
                <!-- Wrapper for Slides -->
                <div class="carousel-inner">
                    @foreach ($data['testimoni']->chunk(2) as $key => $chunk)
                        <div class="item {{ $key == 0 ? "active" : "" }}">
                            @foreach ($chunk as $testimoni)
                                <div class="col-md-6 col-sm-6 mx-auto">
                                    <img src="{{asset('uploads/testimoni/'.$testimoni->file)}}">
                                </div>
                            @endforeach
                        </div> 
                    @endforeach
                </div>

            </div>
        </div>
    </section>
    <!-- Contact Section -->
    <section id="contact">
        <div class="container-fluid wrapper">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-heading">Get in Touch with Us</h2>
                    <h3 class="section-subheading text-muted"> </h3>
                </div>
                    <!--/#portfolio-filter-->
                </div>
            </div>
        </div>
        <div class="portfolio-wrapper portfolio-container-fluid">
            <div class="portfolio-items">
                @foreach ($data['contact'] as $key => $contact)
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 work-grid php graphic">
                        <div class="portfolio-item">
                            <div class="hover-bg">
                                <a href="{{ $contact->link }}" target="_blank">
                                    <div class="hover-text">
                                        <div class="clearfix"></div>
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <img src="{{asset('uploads/contact/'.$contact->file)}}" class="img-responsive" alt="portfolio-image">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Footer -->
    <footer>
        <div class="container-fluid wrapper">
            <div class="col-lg-12 footer-info">

            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 copyright-bottom">
                <span class="copyright">Copyright &copy; {{ date('Y') }} Ariqa Collection. All rights reserved. </span>
            </div>
        </div>
    </footer>
    <!-- Scroll-up -->
    <div class="scroll-up">
        <a href="#header" class="page-scroll"><i class="fa fa-angle-up"></i></a>
    </div>
    <!-- Portfolio Modals -->
    
    <!-- jQuery -->
    <script src="{{asset('assets/frontend/js/jquery.js')}}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>
    <!-- Color Settings script -->
    <script src="{{asset('assets/frontend/js/settings-script.js')}}"></script>
    <!-- Plugin JavaScript -->
    <script src="{{asset('assets/frontend/js/jquery.easing.min.js')}}"></script>
    <!-- Contact Form JavaScript -->
    <script src="{{asset('assets/frontend/js/jqBootstrapValidation.js')}}"></script>
    
    <!-- SmoothScroll script -->
    <script src="{{asset('assets/frontend/js/smoothscroll.js')}}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{asset('assets/frontend/js/xBe.js')}}"></script>
    <!-- Isotope -->
    <script type="text/javascript" src="{{asset('assets/frontend/js/jquery.isotope.min.js')}}"></script>
    <!-- Google Map -->
    <script src="http://maps.googleapis.com/maps/api/js?extension=.js&output=embed"></script>
    <!-- Footer Reveal scirt -->
    <script src="{{asset('assets/frontend/js/footer-reveal.js')}}"></script>

</body>

</html>