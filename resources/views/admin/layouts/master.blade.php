<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ariqa Collection - Admin</title>
    <link href="{{asset('assets/admin/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/paper-dashboard.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/admin/css/demo.css')}}" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{asset('assets/admin/css/themify-icons.css')}}" rel="stylesheet">

	@stack('styles')
</head>
<body>
	<div class="wrapper">
	    <div class="sidebar" data-background-color="white" data-active-color="info">
	    <!--
			Tip 1: you can change the color of the sidebar's background using: data-background-color="white | brown"
			Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
		-->
			<div class="logo">
				<a href="{{ route('dashboard') }}" class="simple-text logo-mini">
					AC
				</a>

				<a href="{{ route('dashboard') }}" class="simple-text logo-normal">
					Ariqa Collection
				</a>
			</div>
			@include('admin.layouts.sidebar')
	    </div>

	    <div class="main-panel">
			<nav class="navbar navbar-default">
	            <div class="container-fluid">
					<div class="navbar-minimize">
						<button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
					</div>
	                <div class="navbar-header">
	                    <button type="button" class="navbar-toggle">
	                        <span class="sr-only">Toggle navigation</span>
	                        <span class="icon-bar bar1"></span>
	                        <span class="icon-bar bar2"></span>
	                        <span class="icon-bar bar3"></span>
	                    </button>
	                </div>
	                <div class="collapse navbar-collapse">

	                    <ul class="nav navbar-nav navbar-right">
	                        <li class="dropdown">
	                            <a href="#user" class="dropdown-toggle" data-toggle="dropdown">
	                                <i class="ti-user"></i>
	                                <span class="notification"> {{ Auth::user()->name }} </span>
									<p class="hidden-md hidden-lg">
										<b class="caret"></b>
									</p>
	                            </a>
	                            <ul class="dropdown-menu">
	                                <li>
										<a href="{{ route('auth.logout') }}">Logout</a>
									</li>
	                            </ul>
	                        </li>
	                    </ul>
	                </div>
	            </div>
	        </nav>

	        <div class="content">
				@yield('content')
	        </div>
			
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright text-center">
                        Copyright © {{ date('Y') }} Ariqa Collection. All rights reserved.
                    </div>
                </div>
            </footer>
	    </div>
	</div>
</body>
	<script src="{{asset('assets/admin/js/jquery.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/admin/js/jquery-ui.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/admin/js/perfect-scrollbar.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/admin/js/bootstrap.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/admin/js/paper-dashboard.js')}}"></script>

	@stack('scripts')
</html>