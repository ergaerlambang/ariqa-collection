<div class="sidebar-wrapper">
    <ul class="nav">
        <li class="{{ \Request::is('admin') ? 'active' : '' }}">
            <a href="{{ route('dashboard') }}">
                <i class="ti-panel"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="{{ \Request::is('admin/banner*') ? 'active' : '' }}">
            <a data-toggle="collapse" href="#banner">
                <i class="ti-layout-slider"></i>
                <p>
                    Banner
                    <b class="caret"></b>
                </p>
            </a>
            <div class="collapse {{ \Request::is('admin/banner*') ? 'in' : '' }}" id="banner">
                <ul class="nav">
                    <li class="{{ \Request::is('admin/banner') ? 'active' : '' }}">
                        <a href="{{ route('banner') }}">
                            <span class="sidebar-mini">LB</span>
                            <span class="sidebar-normal">List Banner</span>
                        </a>
                    </li>
                    <li class="{{ \Request::is('admin/banner/create') ? 'active' : '' }}">
                        <a href="{{ route('banner.create') }}">
                            <span class="sidebar-mini">CB</span>
                            <span class="sidebar-normal">Create Banner</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="{{ \Request::is('admin/about*') ? 'active' : '' }}">
            <a data-toggle="collapse" href="#about">
                <i class="ti-info-alt"></i>
                <p>
                    About us
                    <b class="caret"></b>
                </p>
            </a>
            <div class="collapse {{ \Request::is('admin/about*') ? 'in' : '' }}" id="about">
                <ul class="nav">
                    <li class="{{ \Request::is('admin/about') ? 'active' : '' }}">
                        <a href="{{ route('about') }}">
                            <span class="sidebar-mini">LA</span>
                            <span class="sidebar-normal">List About</span>
                        </a>
                    </li>
                    <li class="{{ \Request::is('admin/about/create') ? 'active' : '' }}">
                        <a href="{{ route('about.create') }}">
                            <span class="sidebar-mini">CA</span>
                            <span class="sidebar-normal">Create About</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="{{ \Request::is('admin/product*') ? 'active' : '' }}">
            <a data-toggle="collapse" href="#product">
                <i class="ti-bag"></i>
                <p>
                    Product
                    <b class="caret"></b>
                </p>
            </a>
            <div class="collapse {{ \Request::is('admin/product*') ? 'in' : '' }}" id="product">
                <ul class="nav">
                    <li class="{{ \Request::is('admin/product') ? 'active' : '' }}">
                        <a href="{{ route('product') }}">
                            <span class="sidebar-mini">LP</span>
                            <span class="sidebar-normal">List Product</span>
                        </a>
                    </li>
                    <li class="{{ \Request::is('admin/product/create') ? 'active' : '' }}">
                        <a href="{{ route('product.create') }}">
                            <span class="sidebar-mini">CP</span>
                            <span class="sidebar-normal">Create Product</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="{{ \Request::is('admin/testimonial*') ? 'active' : '' }}">
            <a data-toggle="collapse" href="#testimoni">
                <i class="ti-heart"></i>
                <p>
                    Testimonials
                    <b class="caret"></b>
                </p>
            </a>
            <div class="collapse {{ \Request::is('admin/testimonial*') ? 'in' : '' }}" id="testimoni">
                <ul class="nav">
                    <li class="{{ \Request::is('admin/testimonial') ? 'active' : '' }}">
                        <a href="{{ route('testimoni') }}">
                            <span class="sidebar-mini">LT</span>
                            <span class="sidebar-normal">List Testimoni</span>
                        </a>
                    </li>
                    <li class="{{ \Request::is('admin/testimonial/create') ? 'active' : '' }}">
                        <a href="{{ route('testimoni.create') }}">
                            <span class="sidebar-mini">CT</span>
                            <span class="sidebar-normal">Create Tesimoni</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="{{ \Request::is('admin/contact*') ? 'active' : '' }}">
            <a data-toggle="collapse" href="#contact">
                <i class="ti-help-alt"></i>
                <p>
                    Contact
                    <b class="caret"></b>
                </p>
            </a>

            <div class="collapse {{ \Request::is('admin/contact*') ? 'in' : '' }}" id="contact">
                <ul class="nav">
                    <li class="{{ \Request::is('admin/contact') ? 'active' : '' }}">
                        <a href="{{ route('contact') }}">
                            <span class="sidebar-mini">LC</span>
                            <span class="sidebar-normal">List Contact</span>
                        </a>
                    </li>
                    <li class="{{ \Request::is('admin/contact/create') ? 'active' : '' }}">
                        <a href="{{ route('contact.create') }}">
                            <span class="sidebar-mini">CC</span>
                            <span class="sidebar-normal">Create Contact</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>