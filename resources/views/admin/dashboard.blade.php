@extends('admin.layouts.master')
@push('styles')
    
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" data-background-color="white">
                    <div class="card-content">
                        <div class="text-center">
                            <img src="{{ asset('assets/frontend/img/logo.jpg') }}" alt="Ariqa" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    
@endpush