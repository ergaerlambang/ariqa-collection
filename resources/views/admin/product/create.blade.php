@extends('admin.layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/image-uploader.min.css') }}">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Create Product</h4>
                </div>
                <form method="POST" action="{{ route('product.store') }}" class="form-horizontal uploader" enctype="multipart/form-data">
                    @csrf
                    <div class="card-content">
                        @include('admin.layouts.alert')

                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nama Produk</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Link Produk</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="link">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Is Active</label>
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <input id="checkbox1" type="checkbox" name="is_active">
                                        <label for="checkbox1">
                                            &nbsp;
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Gambar</label>
                                <div class="col-sm-10">
                                    <div class="images" style="padding-top: .5rem;"></div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-info btn-fill">Submit</button>
                    </div>
                </form>
            </div>  <!-- end card -->
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('assets/admin/js/image-uploader.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('.images').imageUploader({
                imagesInputName: "images",
                maxFiles: 1
            });
            $("input[type=file]").attr('accept',"image/*");
        });

    </script>
@endpush