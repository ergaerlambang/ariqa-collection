@extends('admin.layouts.master')

@push('styles')
    
@endpush

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="title">Testimoni Management</h4>
            <br>

            <div class="card">
                <div class="card-content">
                    <div class="toolbar">
                        <div class="columns columns-right pull-right">
                            <a href="{{ route('testimoni.create') }}" class="btn btn-success">
                                Create
                            </a>
                        </div>
                        <br>
                    </div>
                    <div class="fresh-datatables mt-5" style="margin-top:2em;">
                        @include('admin.layouts.alert')
                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Preview</th>
                                    <th class="disabled-sorting">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key => $item)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>
                                            <img src="{{ asset('uploads/testimoni/'.$item->file) }}" alt="testimoni" class="" width="100" height="100">
                                        </td>
                                        <td>
                                            <a href="{{ route('testimoni.edit',$item->id) }}" class="btn btn-simple btn-warning btn-icon edit"><i class="ti-pencil-alt"></i></a>
                                            <form action="{{ route('testimoni.destroy',$item->id) }}" class="form-delete" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-simple btn-danger btn-icon remove" name="delete_modal"><i class="ti-close"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div><!--  end card  -->
        </div> <!-- end col-md-12 -->
    </div> <!-- end row -->
</div>
@endsection

@push('scripts')
<script src="{{asset('assets/admin/js/jquery.datatables.js')}}"></script>
<script>
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
        search: "_INPUT_",
            searchPlaceholder: "Search records",
        }
    });

    var table = $('#datatables').DataTable();
});
</script>
@endpush