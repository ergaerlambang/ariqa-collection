<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Banner;
use App\Contact;
use App\About;
use App\Product;
use App\Testimoni;

class IndexController extends Controller
{
    public function home()
    {
        $data['banner'] = Banner::all();
        $data['contact'] = Contact::latest()->limit(4)->get();
        $data['about'] = About::all();
        $data['testimoni'] = Testimoni::all();
        $data['product'] = Product::where('is_active',true)->get();
        return view('frontend.index',compact('data'));
    }
}
