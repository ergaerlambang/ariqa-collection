<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\About;
use Validator;

class AboutController extends Controller
{
    public function index()
    {
        $data = About::all();
        return view('admin.about.list',compact('data'));
    }

    public function create()
    {
        return view('admin.about.create');
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'description' => 'required',
                'images' => 'required|array',
                "images.*" => "max:5024"
            ]);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    
            $destinationDocument = 'uploads/about/';
            if(!is_dir($destinationDocument)) {
                mkdir($destinationDocument,0777, true);
            }
    
            $dataFile = null;
            if($request->hasFile('images')) {
                $images = $request->file('images');
                foreach($images as $key => $image) {
                    $uploadedDocument = $image;
                    $nameFile = "About"."-".time().rand(11111, 999999).'.'.$uploadedDocument->getClientOriginalExtension();
                    // upload the images
                    $uploadedDocument->move($destinationDocument, $nameFile);
                    // save image path to database
                    $dataFile = $nameFile;
                }
            }
            
            $about = new About;
            $about->description = $request->description;
            $about->file = $dataFile;
            $about->save();
    
            return redirect()->route('about')->with('success','About has been created!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }

    }

    public function edit($id)
    {
        $about = About::find($id);
        return view('admin.about.edit',compact('about'));
    }

    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(),[
                'description' => 'required',
            ]);

            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $about = About::find($id);
            $destinationDocument = 'uploads/about/';
            if(!is_dir($destinationDocument)) {
                mkdir($destinationDocument,0777, true);
            }
    
            if($request->hasFile('images')) {
                $validator = Validator::make($request->all(),[
                    'images' => 'required|array',
                    "images.*" => "max:5024"
                ]);
                
                if($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
        
                $images = $request->file('images');
                foreach($images as $key => $image) {
                    $uploadedDocument = $image;
                    $nameFile = "About"."-".time().rand(11111, 999999).'.'.$uploadedDocument->getClientOriginalExtension();
                    // upload the images
                    $uploadedDocument->move($destinationDocument, $nameFile);
                    // save image path to database
                    $dataFile = $nameFile;
                }
                $about->file = $dataFile;
            }
            $about->description = $request->description;
            $about->update();
    
            return redirect()->route('about')->with('success','About has been Edited!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            About::find($id)->delete();

            return redirect()->route('about')->with('success','About has been Deleted!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function thumbnail(Request $request)
    {
        $about = About::find($request->about_id);
        $preloaded['id'] = 0;
        $preloaded['src'] = asset('uploads/about')."/".$about->file; 
        return response()->json($preloaded,200);
    }
}
