<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Testimoni;
use Validator;

class TestimoniController extends Controller
{
    public function index()
    {
        $data = Testimoni::all();
        return view('admin.testimoni.list',compact('data'));
    }

    public function create()
    {
        return view('admin.testimoni.create');
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'images' => 'required|array',
                "images.*" => "max:5024"
            ]);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    
            $destinationDocument = 'uploads/testimoni/';
            if(!is_dir($destinationDocument)) {
                mkdir($destinationDocument,0777, true);
            }
    
            $dataFile = null;
            if($request->hasFile('images')) {
                $images = $request->file('images');
                foreach($images as $key => $image) {
                    $uploadedDocument = $image;
                    $nameFile = "Testimoni"."-".time().rand(11111, 999999).'.'.$uploadedDocument->getClientOriginalExtension();
                    // upload the images
                    $uploadedDocument->move($destinationDocument, $nameFile);
                    // save image path to database
                    $dataFile = $nameFile;
                }
            }
            
            $testimoni = new Testimoni;
            $testimoni->file = $dataFile;
            $testimoni->save();
    
            return redirect()->route('testimoni')->with('success','Testimoni has been created!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }

    }

    public function edit($id)
    {
        $testimoni = Testimoni::find($id);
        return view('admin.testimoni.edit',compact('testimoni'));
    }

    public function update(Request $request, $id)
    {
        try {

            $testimoni = Testimoni::find($id);
            $destinationDocument = 'uploads/testimoni/';
            if(!is_dir($destinationDocument)) {
                mkdir($destinationDocument,0777, true);
            }
    
            if($request->hasFile('images')) {
                $validator = Validator::make($request->all(),[
                    'images' => 'required|array',
                    "images.*" => "max:5024"
                ]);
                
                if($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
        
                $images = $request->file('images');
                foreach($images as $key => $image) {
                    $uploadedDocument = $image;
                    $nameFile = "Product"."-".time().rand(11111, 999999).'.'.$uploadedDocument->getClientOriginalExtension();
                    // upload the images
                    $uploadedDocument->move($destinationDocument, $nameFile);
                    // save image path to database
                    $dataFile = $nameFile;
                }
                $testimoni->file = $dataFile;
            }
            $testimoni->update();
    
            return redirect()->route('testimoni')->with('success','Testimoni has been Edited!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            Testimoni::find($id)->delete();
            return redirect()->route('testimoni')->with('success','Testimoni has been Deleted!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function thumbnail(Request $request)
    {
        $testimoni = Testimoni::find($request->testimoni_id);
        $preloaded['id'] = 0;
        $preloaded['src'] = asset('uploads/testimoni')."/".$testimoni->file; 
        return response()->json($preloaded,200);
    }
}
