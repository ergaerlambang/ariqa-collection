<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contact;
use Validator;

class ContactController extends Controller
{
    public function index()
    {
        $data = Contact::all();
        return view('admin.contact.list',compact('data'));
    }

    public function create()
    {
        return view('admin.contact.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'link' => 'required|url'
        ]);
        
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {

            $contact = new Contact;
            $destinationDocument = 'uploads/contact/';
            if(!is_dir($destinationDocument)) {
                mkdir($destinationDocument,0777, true);
            }
    
            if($request->hasFile('images')) {
                $validator = Validator::make($request->all(),[
                    'images' => 'required|array',
                    "images.*" => "max:5024"
                ]);
                
                if($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
        
                $images = $request->file('images');
                foreach($images as $key => $image) {
                    $uploadedDocument = $image;
                    $nameFile = "Contact"."-".time().rand(11111, 999999).'.'.$uploadedDocument->getClientOriginalExtension();
                    // upload the images
                    $uploadedDocument->move($destinationDocument, $nameFile);
                    // save image path to database
                    $dataFile = $nameFile;
                }
                $contact->file = $dataFile;
            }
            $contact->link = $request->link;
            $contact->name = $request->name;
            $contact->save();
    
            return redirect()->route('contact')->with('success','Contact has been Created!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function edit($id)
    {
        $contact = Contact::find($id);
        return view('admin.contact.edit',compact('contact'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'link' => 'required|url'
        ]);
        
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {

            $contact = Contact::find($id);
            $destinationDocument = 'uploads/contact/';
            if(!is_dir($destinationDocument)) {
                mkdir($destinationDocument,0777, true);
            }
    
            if($request->hasFile('images')) {
                $validator = Validator::make($request->all(),[
                    'images' => 'required|array',
                    "images.*" => "max:5024"
                ]);
                
                if($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
        
                $images = $request->file('images');
                foreach($images as $key => $image) {
                    $uploadedDocument = $image;
                    $nameFile = "Contact"."-".time().rand(11111, 999999).'.'.$uploadedDocument->getClientOriginalExtension();
                    // upload the images
                    $uploadedDocument->move($destinationDocument, $nameFile);
                    // save image path to database
                    $dataFile = $nameFile;
                }
                $contact->file = $dataFile;
            }
            $contact->link = $request->link;
            $contact->update();
    
            return redirect()->route('contact')->with('success','Contact has been Edited!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            Contact::find($id)->delete();
            return redirect()->route('contact')->with('success','Contact has been Deleted!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function thumbnail(Request $request)
    {
        $contact = Contact::find($request->contact_id);
        $preloaded['id'] = 0;
        $preloaded['src'] = asset('uploads/contact')."/".$contact->file; 
        return response()->json($preloaded,200);
    }
}
