<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Validator;

class ProductController extends Controller
{
    public function index()
    {
        $data = Product::all();
        return view('admin.product.list',compact('data'));
    }

    public function create()
    {
        return view('admin.product.create');
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'link' => 'required|url',
                'images' => 'required|array',
                "images.*" => "max:5024"
            ]);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    
            $destinationDocument = 'uploads/product/';
            if(!is_dir($destinationDocument)) {
                mkdir($destinationDocument,0777, true);
            }
    
            $dataFile = null;
            if($request->hasFile('images')) {
                $images = $request->file('images');
                foreach($images as $key => $image) {
                    $uploadedDocument = $image;
                    $nameFile = "Product"."-".time().rand(11111, 999999).'.'.$uploadedDocument->getClientOriginalExtension();
                    // upload the images
                    $uploadedDocument->move($destinationDocument, $nameFile);
                    // save image path to database
                    $dataFile = $nameFile;
                }
            }
            
            $product = new Product;
            $product->name = $request->name;
            $product->link = $request->link;
            $product->is_active = isset($request->is_active) ? true : false;
            $product->file = $dataFile;
            $product->save();
    
            return redirect()->route('product')->with('success','Product has been created!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }

    }

    public function edit($id)
    {
        $product = Product::find($id);
        return view('admin.product.edit',compact('product'));
    }

    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'link' => 'required',
            ]);

            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $product = Product::find($id);
            $destinationDocument = 'uploads/product/';
            if(!is_dir($destinationDocument)) {
                mkdir($destinationDocument,0777, true);
            }
    
            if($request->hasFile('images')) {
                $validator = Validator::make($request->all(),[
                    'images' => 'required|array',
                    "images.*" => "max:5024"
                ]);
                
                if($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
        
                $images = $request->file('images');
                foreach($images as $key => $image) {
                    $uploadedDocument = $image;
                    $nameFile = "Product"."-".time().rand(11111, 999999).'.'.$uploadedDocument->getClientOriginalExtension();
                    // upload the images
                    $uploadedDocument->move($destinationDocument, $nameFile);
                    // save image path to database
                    $dataFile = $nameFile;
                }
                $product->file = $dataFile;
            }
            $product->name = $request->name;
            $product->link = $request->link;
            $product->is_active = isset($request->is_active) ? true : false;
            $product->update();
    
            return redirect()->route('product')->with('success','Product has been Edited!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            Product::find($id)->delete();
            return redirect()->route('product')->with('success','Product has been Deleted!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function thumbnail(Request $request)
    {
        $product = Product::find($request->product_id);
        $preloaded['id'] = 0;
        $preloaded['src'] = asset('uploads/product')."/".$product->file; 
        return response()->json($preloaded,200);
    }
}
