<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Banner;
use Validator;

class BannerController extends Controller
{
    public function index()
    {
        $data = Banner::all();
        return view('admin.banner.list',compact('data'));
    }

    public function create()
    {
        return view('admin.banner.create');
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'images' => 'required|array',
                "images.*" => "max:5024"
            ]);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    
            $destinationDocument = 'uploads/banner/';
            if(!is_dir($destinationDocument)) {
                mkdir($destinationDocument,0777, true);
            }
    
            $dataFile = null;
            if($request->hasFile('images')) {
                $images = $request->file('images');
                foreach($images as $key => $image) {
                    $uploadedDocument = $image;
                    $nameFile = "Banner"."-".time().rand(11111, 999999).'.'.$uploadedDocument->getClientOriginalExtension();
                    // upload the images
                    $uploadedDocument->move($destinationDocument, $nameFile);
                    // save image path to database
                    $dataFile = $nameFile;
                }
            }
    
            $banner = new Banner;
            $banner->title = "Fashion Store";
            $banner->description = "Ariqa Collection";
            $banner->file = $dataFile;
            $banner->save();
    
            return redirect()->route('banner')->with('success','Banner has been created!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }

    }

    public function edit($id)
    {
        $banner = Banner::find($id);
        return view('admin.banner.edit',compact('banner'));
    }

    public function update(Request $request, $id)
    {
        try {
            // return $request;
            $banner = Banner::find($id);
            $destinationDocument = 'uploads/banner/';
            if(!is_dir($destinationDocument)) {
                mkdir($destinationDocument,0777, true);
            }
    
            if($request->hasFile('images')) {
                $validator = Validator::make($request->all(),[
                    'images' => 'required|array',
                    "images.*" => "max:5024"
                ]);
                
                if($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
        
                $images = $request->file('images');
                foreach($images as $key => $image) {
                    $uploadedDocument = $image;
                    $nameFile = "Banner"."-".time().rand(11111, 999999).'.'.$uploadedDocument->getClientOriginalExtension();
                    // upload the images
                    $uploadedDocument->move($destinationDocument, $nameFile);
                    // save image path to database
                    $dataFile = $nameFile;
                }
                $banner->file = $dataFile;
            }
            $banner->update();
    
            return redirect()->route('banner')->with('success','Banner has been Edited!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            Banner::find($id)->delete();

            return redirect()->route('banner')->with('success','Banner has been Deleted!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function thumbnail(Request $request)
    {
        $banner = Banner::find($request->banner_id);
        $preloaded['id'] = 0;
        $preloaded['src'] = asset('uploads/banner')."/".$banner->file; 
        return response()->json($preloaded,200);
    }
}
